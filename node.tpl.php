  <article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div class="post">
  <header>
  <h2 class="posttitle"><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
  </header>
  <div class="postdate"><p><span class="postdateno"><?php print $date_day ?></span><br /><?php print $date_month ?></p></div>
  <div class="postcontent"><?php print $content;?></div>
<div class="postdetails">
<p class="postedby"><span class="sep">Posted On </span><time class="entry-date" datetime="%2$s" pubdate><?php print $date_month ?> <?php print $date_day ?>,<?php print $date_year ?></time><span class="sep"> by </span><span class="author vcard"><?php print theme('username', $node) ?></span></p>
  </article> <!-- /.node -->

