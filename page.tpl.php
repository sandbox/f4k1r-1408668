<?php
 /**
  * @file
  */
?>
<!DOCTYPE html>
<head>
<title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts;?>    
</head>

<body class="home blog">

<div id="container">
<header id="top">
	<ul id="social">
	<li class="twitter"><a href="#">Twitter</a></li><li class="rss"><a href="#">RSS</a></li><li class="facebook"><a href="#">Facebook</a></li></ul>
	<p class="subscribers">Subscribers <span class="subscribersno">1446</span></p>	<p>	
	<a href="<?php print check_url($front_page); ?>" title="Busby Theme" rel="home">
	<img src="<?php print $logo;?>" class="logo" alt="logo"/></a></p>
</header>

<nav>
<div id="access" role="navigation">
		<div class="menu"><?php print theme('links', $primary_links); ?></div>
</div><!-- #access -->

<?php print $search_box;?>
</nav>



<div id="main" role="main" class="clearfix"><div id="left">
<?php print $tabs;?>
<?php print $messages;?>
<?php print $content;?>
</div>


<div id="right">
<?php print $content_right;?>
</div>

		
</div><footer>

<div id="footer">
<div class="footercol"><?php print $footer_col_1;?></div><div class="footercol"><?php print $footer_col_2;?></div><div class="footercol"><?php print $footer_col_3;?></div><div class="footercol"><?php print $footer_col_4;?></div>
<div id="copyright">
<p>&copy; 2012 Busby Theme. <a href="http://wplift.com">Busby Theme by WPLIft for Wordpress. Ported to Drupal by f4k1r</a></p>
 
</div>

</div>
</footer>
</div> <!--! end of #container -->

</body>
</html>

