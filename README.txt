Busby Wordpress Port to Drupal.
This theme is intended to be used on Blogs. The wordpress theme was originally
developed by http://wplift.com and releases under GPLv3.

Installation:
Copy the files of the theme into your themes directory.
Go to admin/build/themes and enable the theme.
If you want to have the subscribers field automated, look into this module
http://drupal.org/project/subscounter


To Do:
1.) Make busby a subtheme of Fusion enabling all the wonderful options that 
    carry on from Fusion Core
2.) D7 port of Busby (High Priority)

